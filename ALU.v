module ALU(Ain, Bin, ALUop, ALUout, ALUs);
	
	//Defining I/O
	input [15:0] Ain, Bin;
	input [1:0] ALUop;
	output reg [15:0] ALUout;
	reg a2;
	output [2:0] ALUs;
	
	//Defining Supported Opeations 
	`define ADD 2'b00
	`define SUBTRACT 2'b01
	`define AND 2'b10
	`define NEGATE 2'b11
	
	
	
	always @(*) begin
		//Preforms the requested operation
		case(ALUop)
			`ADD: 		ALUout = Ain + Bin;
			`SUBTRACT: 	ALUout = Ain - Bin;
			`AND: 		ALUout = Ain & Bin;
			`NEGATE: 	ALUout = ~Bin;
			default 	ALUout = 16'b0;
		endcase
	end
	
	always @(*) begin
		case(ALUop)
			`ADD:		a2 = (Ain[15] ~^ Bin[15]) & (ALUout[15] ^ Ain[15]);
			`SUBTRACT:	a2 = (Ain[15] ^ Bin[15]) & (ALUout[15] ~^ Bin[15]);
			2'b1x:		a2 = 1'b0;
			default		a2 = 1'b0;
		endcase
	end
	
	// status signal block
	assign ALUs[2] = a2;
	assign ALUs[1] = ALUout[15];
	assign ALUs[0] = ~|ALUout;

endmodule
