module datapath (datapath_in, vsel, asel, bsel, writenum, write, readnum, clk, loada, loadb, loadc, loads, shift, ALUop, status,sximm5, sximm8, loada_out, loadb_out, datapath_out, R0out, pc_out);

	//Defining I/O
	input [15:0] datapath_in, sximm5, sximm8, datapath_in;
	input [7:0] pc_out;
	input [3:0] vsel;
	input [2:0] writenum, readnum;
	input [1:0] ALUop, shift;
	input asel, bsel, loada, loadb, loadc, loads, clk, write;
	
	
	output [15:0]datapath_out, R0out;
	output [2:0]status;
	
	output [15:0]loada_out;
	output [15:0]loadb_out;
	
	//Defining the wires that will be used
	
	wire [15:0] datafile_out, data_in, loada_out, loadb_out, shifter_out, Ain, Bin, ALUout;
	wire [2:0]ALUs;

	
	//Initial Mux that will determine the value of the registry data input. 
	Mux4  #(16) V_Selector(datapath_in, sximm8, {8'b0, pc_out}, datapath_out, vsel, data_in);
	
	//Registry 
	regfile Register_File(writenum, write, data_in, clk, readnum, datafile_out, R0out);
	
	//The 2 Load Enabled registers taht can hold up to 2 outputs from the Register File (one in each)
	RegN #(16)   Load_Register_A(datafile_out,loada,clk,loada_out);
	RegN #(16)   Load_Register_B(datafile_out,loadb,clk,loadb_out);
	
	shifter shifter_module(loadb_out, shift, shifter_out);
	
	Mux2  #(16) A_Selector(loada_out, 16'b0, asel, Ain);
	Mux2  #(16) B_Selector(shifter_out, sximm5, bsel, Bin);
	
	ALU  Arithmetic_Logic_Unit(Ain, Bin, ALUop, ALUout, ALUs);
	
	RegN #(16)  Load_Register_C(ALUout,loadc,clk,datapath_out);
	RegN #(3)   Load_Register_Status(ALUs,loads,clk,status);
endmodule