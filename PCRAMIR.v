//from lab 5-6
module PCRAMIR(clk, reset, mwrite, msel, 
//loadpc, 
loadir, C, B, mdata, IR_out, pc_out,
//from lab 7
sximm8, A, tsel, incp, execb, status, cond, isBL);
	
	//Defining I/O for the module
	//lab5-6
	input clk, reset, mwrite, msel, loadir;				//loadpc;
	input [15:0] B, C;
	//lab7
	input [15:0] sximm8, A;
	input tsel, incp, execb, isBL;
	input [2:0] status, cond;
	
	output [15:0] mdata , IR_out;
	output [7:0] pc_out;
	
	
	//Wires to be used
	//lab5-6
	wire [7:0] pc_reset, pc_in, address;
	wire [15:0] mdata;
	//lab7
	wire [7:0] pcrel, pctg, pc_next, pc1;
	wire taken, loadpc;
	
	//lab7 wiring
	assign pcrel = pc_out + sximm8[7:0];
	assign pctg = tsel ? pcrel : A[7:0];
	assign pc1 = pc_out + 8'b1;
	assign pc_next = incp ? pc1 : pctg;
	
	BranchUnit branch(execb, cond, status, taken);
	
	assign loadpc = incp | taken | isBL;
	
	//lab5-6 wiring
//	Mux2  #(8) Update_Pc(pc_out, pc_next, loadpc, pc_reset);
	assign pc_reset = loadpc ? pc_next : pc_out;
	
//	Mux2  #(8) Reset_Pc(pc_reset, 8'b00000000, reset, pc_in);
	assign pc_in = reset ? 8'b0 : pc_reset;
	
	vDFF #(8) PC(clk, pc_in, pc_out);
	
//	Mux2  #(8) Address_Selector(pc_out, C[7:0], msel, address);
	assign address = msel ? C[7:0] : pc_out;
	
	RAM #(16, 8) Memory(clk, address, address, mwrite, B, mdata);

	RegN #(16) IR(mdata, loadir, clk, IR_out);

endmodule

module RAM(clk,read_address,write_address,write,din,dout);
	parameter data_width = 32; 
	parameter addr_width = 4;
	parameter filename = "Test_Basic.txt";
//	parameter filename = "data_fibonly_rename_to_use";
	
	input clk;
	input [addr_width-1:0] read_address, write_address;
	input write;
	input [data_width-1:0] din;
	output [data_width-1:0] dout;
	reg [data_width-1:0] dout;
	
	reg [data_width-1:0] mem [2**addr_width-1:0];
	
	initial $readmemb(filename, mem);
	
	always @ (posedge clk) begin
		if (write)
		mem[write_address] <= din;
		dout <= mem[read_address]; 		// dout doesn't get din in this clock cycle 
	end									// (this is due to Verilog non-blocking assignment "<=")
endmodule

module BranchUnit(execb, cond, status, taken);
	input execb;
	input [2:0] cond, status;
	
	output taken;
	
	
	wire V, N, Z;
	reg branch_out;
	
	assign {V, N, Z} = status[2:0];
	
	`define B	3'b000
	`define BEQ	3'b001
	`define BNE	3'b010
	`define BLT	3'b011
	`define BLE	3'b100	
	
	always @(*) begin
		case(cond)
		`B		: branch_out = 1'b1;
		`BEQ	: branch_out = Z;
		`BNE	: branch_out = ~Z;
		`BLT	: branch_out = (N ^ V);
		`BLE	: branch_out = ((N ^ V) | Z);
		default	: branch_out = 1'b0;
		endcase
	end
		
	assign taken = execb & branch_out;	
	
endmodule