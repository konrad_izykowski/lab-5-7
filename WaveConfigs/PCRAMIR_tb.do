onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Reading Values}
add wave -noupdate /PCRAMIR_tb/clk
add wave -noupdate /PCRAMIR_tb/reset
add wave -noupdate /PCRAMIR_tb/loadpc
add wave -noupdate /PCRAMIR_tb/dut/PC/out
add wave -noupdate /PCRAMIR_tb/dut/mdata
add wave -noupdate /PCRAMIR_tb/dut/Memory/read_address
add wave -noupdate /PCRAMIR_tb/loadir
add wave -noupdate /PCRAMIR_tb/dut/IR_out
add wave -noupdate -divider {Writing Test}
add wave -noupdate /PCRAMIR_tb/clk
add wave -noupdate /PCRAMIR_tb/mwrite
add wave -noupdate /PCRAMIR_tb/C
add wave -noupdate /PCRAMIR_tb/B
add wave -noupdate {/PCRAMIR_tb/dut/Memory/mem[2]}
add wave -noupdate /PCRAMIR_tb/mdata
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {100 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 243
configure wave -valuecolwidth 160
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {24 ps} {104 ps}
