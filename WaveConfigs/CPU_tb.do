onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Inputs
add wave -noupdate /CPU_tb/clk
add wave -noupdate /CPU_tb/reset
add wave -noupdate /CPU_tb/opcode
add wave -noupdate /CPU_tb/op
add wave -noupdate -divider {To Decoder}
add wave -noupdate /CPU_tb/nsel
add wave -noupdate -divider {To PCRAMIR}
add wave -noupdate /CPU_tb/mwrite
add wave -noupdate /CPU_tb/msel
add wave -noupdate /CPU_tb/loadpc
add wave -noupdate /CPU_tb/loadir
add wave -noupdate -divider {To Datapath}
add wave -noupdate /CPU_tb/vsel
add wave -noupdate /CPU_tb/loada
add wave -noupdate /CPU_tb/loadb
add wave -noupdate /CPU_tb/asel
add wave -noupdate /CPU_tb/bsel
add wave -noupdate /CPU_tb/loadc
add wave -noupdate /CPU_tb/loads
add wave -noupdate /CPU_tb/write
add wave -noupdate -divider States
add wave -noupdate /CPU_tb/DUT/state
add wave -noupdate /CPU_tb/DUT/state_next
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {65 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 265
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {14 ps} {68 ps}
