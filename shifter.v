module shifter(in, shift, out) ;
	input [15:0] in ;
	input [1:0] shift ;
	output [15:0] out ;
	reg [15:0] out ;
	
	always @(*) begin
		case(shift)
			2'b00: out = in ;
			2'b01: out = {in[14:0], 1'b0} ;
			2'b10: out = {1'b0, in[15:1]} ;
			2'b11: out = {in[15], in [15:1]} ;
			default out = in ;
		endcase
	end	
	
endmodule