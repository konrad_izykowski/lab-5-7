module Mux4 (a3, a2, a1, a0, s, out);

	parameter k =16;
	
	input [k-1:0] a3, a2 ,a1, a0;
	input [3:0] s;
	
	output [k-1:0] out;
	
	assign out =  (a3 & {k{s[3]}}) | (a2 & {k{s[2]}}) | (a1 & {k{s[1]}}) | (a0 & {k{s[0]}});
	
endmodule

module Mux3 (a2, a1, a0, s, out);

	parameter k =3;
	
	input [k-1:0] a2 ,a1, a0;
	input [2:0] s;
	
	output [k-1:0] out;
	
	assign out =  (a2 & {k{s[2]}}) | (a1 & {k{s[1]}}) | (a0 & {k{s[0]}});
	
endmodule