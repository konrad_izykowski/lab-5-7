module regfile_tb () ; //writenum, write, data_in, clk, readnum, data_out
	reg [15:0] data_in ;
	reg [2:0] writenum, readnum ;
	reg write, clk ;
	wire [15:0] data_out ;
	
	regfile dut(writenum, write, data_in, clk, readnum, data_out);
	
		initial begin
			clk = 0; #5;
			forever begin
				clk = 1; #5;
				clk = 0; #5;
			end
		end

		initial begin
		
			//assigns values to all 8 registers and reads from them afterwards
				data_in = 16'b0000111100001111; write = 1; writenum = 0; readnum = 0;
				#10
				
				data_in = 16'b0011001100110011; write = 1; writenum = 1; readnum = 0;
				#1
				assert	(data_out == 16'b0000111100001111) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b0101010101010101; write = 1; writenum = 2; readnum = 1;
				#1
				assert	(data_out == 16'b0011001100110011) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b1010101010101010; write = 1; writenum = 3; readnum = 2;
				#1
				assert	(data_out == 16'b0101010101010101) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b1100110011001100; write = 1; writenum = 4; readnum = 3;
				#1
				assert	(data_out == 16'b1010101010101010) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b1111000011110000; write = 1; writenum = 5; readnum = 4;
				#1
				assert	(data_out == 16'b1100110011001100) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b1100101001010011; write = 1; writenum = 6; readnum = 5;
				#1
				assert	(data_out == 16'b1111000011110000) $display("PASS"); else $error("FAIL");
				#9
				
				data_in = 16'b0100110001110000; write = 1; writenum = 7; readnum = 6;
				#1
				assert	(data_out == 16'b1100101001010011) $display("PASS"); else $error("FAIL");
				#9
				
				readnum = 7;
				#1
				assert	(data_out == 16'b0100110001110000) $display("PASS"); else $error("FAIL");
				#9
			
			
			//checks that values do not get changed when write is off		
				data_in = 16'b0000000000000000; write = 0; writenum = 0; readnum = 0;
				#10
				assert	(data_out != 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				write = 1; writenum = 2; readnum = 2;
				#10
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				write = 0; writenum = 4; readnum = 4;
				#10
				assert	(data_out != 16'b0000000000000000) $display("PASS"); else $error("FAIL");
							
				write = 1; writenum = 6; readnum = 6;
				#10
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				write = 0; writenum = 3; readnum = 3;
				#10
				assert	(data_out != 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				write = 1; writenum = 5; readnum = 5;
				#10
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
			
			
			//checks values that were stored earlier are still held in the registry, write is off
				data_in = 16'b1111111111111111; write = 0; 
				
				writenum = 0;
				#9
				readnum = 0;
				#1
				assert	(data_out == 16'b0000111100001111) $display("PASS"); else $error("FAIL");
				
				writenum = 1;
				#9
				readnum = 1;
				#1
				assert	(data_out == 16'b0011001100110011) $display("PASS"); else $error("FAIL");
				
				writenum = 2;
				#9
				readnum = 2;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				writenum = 3;
				#9
				readnum = 3;
				#1
				assert	(data_out == 16'b1010101010101010) $display("PASS"); else $error("FAIL");
				
				writenum = 4;
				#9
				readnum = 4;
				#1
				assert	(data_out == 16'b1100110011001100) $display("PASS"); else $error("FAIL");
				
				writenum = 5;
				#9
				readnum = 5;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				writenum = 6;
				#9
				readnum = 6;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");
				
				writenum = 7;
				#9
				readnum = 7;
				#1
				assert	(data_out == 16'b0100110001110000) $display("PASS"); else $error("FAIL");
			
			
			//checks that data_out changes as soon as readnum changes, not dependant on clk
				readnum = 0;
				#1
				assert	(data_out == 16'b0000111100001111) $display("PASS"); else $error("FAIL");
				
				readnum = 1;
				#1
				assert	(data_out == 16'b0011001100110011) $display("PASS"); else $error("FAIL");

				readnum = 2;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");

				readnum = 3;
				#1
				assert	(data_out == 16'b1010101010101010) $display("PASS"); else $error("FAIL");

				readnum = 4;
				#1
				assert	(data_out == 16'b1100110011001100) $display("PASS"); else $error("FAIL");

				readnum = 5;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");

				readnum = 6;
				#1
				assert	(data_out == 16'b0000000000000000) $display("PASS"); else $error("FAIL");

				readnum = 7;
				#1
				assert	(data_out == 16'b0100110001110000) $display("PASS"); else $error("FAIL");
			
			$stop;
		end
endmodule