module CPU(clk, reset, opcode, op, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL);  
//formatted clk | inputs | outputs to decoder |outputs to PCRIMR| outputs to DP 



	input[2:0] opcode;
	input[1:0] op;
	input reset, clk;
	
	output reg [3:0] vsel;
	output reg [2:0] nsel;
	output reg loadir, asel, bsel, loada, loadb, loadc, loads, write, msel, mwrite, incp, tsel, execb,isBL;
	
	wire [3:0] state;
	wire [3:0] state_next;
	
	reg [3:0] next_state;
	
	//Defining States
	`define SR   4'b1111
	`define SLIR 4'b0000
	`define SLPC 4'b0001
	`define SCDC 4'b0010
	`define SWRN 4'b0011
	`define SRRM 4'b0100
	`define SALU 4'b0101
	`define SRFR 4'b0110
	`define SWTR 4'b0111
	`define SBR1 4'b1000
	`define SBR2 4'b1001
	`define SHLT 4'b1010
	`define SWFR 4'b1011
	
	
	
	vDFF #(4) State_Switcher(clk, state_next,state);
	
	//Checks for reset
	assign state_next = reset ? `SR : next_state;
	
	always @(*) begin
		casex({state,opcode,op})
		
		//Common All
		{`SR,5'bx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SLIR,5'bx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLPC, 3'b100, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SLPC,5'bx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {((opcode != 3'b111) ? `SCDC : `SHLT), 3'b100, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Move One
		{`SCDC,5'b11010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SWRN,5'b11010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		
		//ALU Instructions
		{`SCDC,5'b101xx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SRRM, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SRRM,5'b101xx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SALU, 3'b001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b10100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b10110} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b10111} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SWRN,5'b101xx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		{`SALU,5'b10101} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b0, 1'b0};
		
		//Move Two
		{`SCDC,5'b11000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SRRM, 3'b001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SRRM,5'b11000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SALU, 3'b001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b11000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b1, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SWRN,5'b11000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		
		//LDR
		{`SCDC,5'b01100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SALU, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b1000, 1'b1, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b01100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SRFR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b1000, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SRFR,5'b01100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b010, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b1000, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SWRN,5'b01100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b1000, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		
		//STR
		{`SCDC,5'b10000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SRRM, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b1, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SRRM,5'b10000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SALU, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b1, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SALU,5'b10000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWTR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0};
		{`SWTR,5'b10000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWFR, 3'b010, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SWFR,5'b10000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Normal Branching Support 
		{`SCDC,5'b00100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR1, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 4'b0100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SBR1,5'b00100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR2, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 4'b0100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SBR2,5'b00100} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 4'b0100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Branch link support
		{`SCDC,5'b01011} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR1, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		{`SBR1,5'b01011} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR2, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1};
		{`SBR2,5'b11011} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Branch Link x Support
		{`SCDC,5'b01010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SWRN, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0010, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SWRN,5'b01010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR1, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0};
		{`SBR1,5'b01010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR2, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1};
		{`SBR2,5'b11010} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Branch x Support
		{`SCDC,5'b01000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR1, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0010, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		{`SBR1,5'b01000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SBR2, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1};
		{`SBR2,5'b11000} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SLIR, 3'b010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0010, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Halt Support
		{`SHLT,5'bx} : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL} = {`SHLT, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		
		//Default in case of Malformed instructions
		default : {next_state, nsel, msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write,isBL} = {`SLIR, 3'b100, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0001, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
		

		endcase
	end
	
endmodule