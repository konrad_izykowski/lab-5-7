module datapath_tb();

	reg clk, asel, bsel, vsel, loada, loadb, loadc, loads, write;
	reg [2:0] writenum, readnum;
	reg [1:0]shift, ALUop;
	reg [15:0] datapath_in;
	
	wire[15:0] datapath_out;
	
	datapath dut(datapath_in, vsel, writenum, write, readnum, clk, loada, loadb, shift, asel, bsel, ALUop, loadc, loads, datapath_out,status);

	initial begin
		clk = 0; #5;
		forever begin
			clk = 1; #5;
			clk = 0; #5;
		end
	end
	
	initial begin
	
	//Requiered Test
			//setting all values to 0
			loada = 1'b0; loadb = 1'b0; readnum = 3'b000; shift = 2'b00; asel = 0; bsel = 0; ALUop = 2'b00;
			//adding test values to the register file
				//adding 7 to R0
				vsel = 1'b1;
				datapath_in = 16'b0000000000000111; writenum = 3'b000; write = 1'b1;
				#10
				assert (datapath_tb.dut.Register_File.R0out == 16'b0000000000000111) $display("Register 0 PASS"); else $error("Register 0 FAIL");
				//adding 1 to R1
				writenum = 3'b001; datapath_in =  16'b0000000000000010; 
				#10
				assert (datapath_tb.dut.Register_File.R1out == 16'b0000000000000010) $display("Register 1 PASS"); else $error("Register 1 FAIL");
				//reading 2 and storing it in Load_Register_A
				readnum = 3'b001; loada = 1;
				#10
				 loada = 0;
				//reading 2 and storing it in Load_Register_B
				readnum = 3'b000; loadb = 1;
				#10
				loadb = 0;
				//shifting 7 to 14 and adding the values
				shift = 2'b01; 
				ALUop = 2'b00;
				#10
				loadc = 1'b1;
				loads = 1'b1;
				#10
				vsel = 1'b0; writenum = 3'b010; write = 1'b1;
				#20
				assert (datapath_tb.dut.Register_File.R2out == 16'b0000000000010000) $display("Register 2 PASS"); else $error("Register 2 FAIL");
				assert (status == 0) $display("Status 2 PASS"); else $error("Status 2 FAIL");
				write = 1'b0;
				#10
			//Checking Vsel
				assert (datapath_tb.dut.data_in == 16'b0000000000010000) $display("Vsel select 0 PASS"); else $error("Vsel select 0 FAIL");
				vsel = 1'b1;
				datapath_in = 16'b0000000000000111;
				#10
				assert (datapath_tb.dut.data_in == 16'b0000000000000111) $display("Vsel select 1 PASS"); else $error("Vsel select 1 FAIL");
				#10
			//inserting values in R3 and R4
				datapath_in = 16'b0000000000000100;
				write = 1'b1; writenum = 3'b011;
				#10
				assert (datapath_tb.dut.Register_File.R3out == 16'b0000000000000100) $display("Register 3 PASS"); else $error("Register 3 FAIL");
				writenum = 3'b100;
				datapath_in = 16'b0000000000001000;
				#10
				assert (datapath_tb.dut.Register_File.R4out == 16'b0000000000001000) $display("Register 4 PASS"); else $error("Register 4 FAIL");
				#10
			//loading a and b
				readnum = 3'b011;
				loada = 1'b1;
				#10
				assert (datapath_tb.dut.loada_out == 16'b0000000000000100) $display("Load A PASS"); else $error("Load A FAIL");
				loada = 1'b0;
				readnum = 3'b100;
				loadb = 1'b1;
				#10
				assert (datapath_tb.dut.loadb_out == 16'b0000000000001000) $display("Load B PASS"); else $error("Load B FAIL");
				shift = 2'b10;
				loadc = 1'b1;
				loads = 1'b1;
				#10
				assert (datapath_out == 16'b0000000000001000) $display("Shifter divide by 2 PASS"); else $error("Shifter divide by 2 FAIL");
				loadc = 1'b0;
				loads = 1'b0;
				#10
				shift = 2'b00;
				loadc = 1'b1;
				loads = 1'b1;
				#10
				assert (datapath_out == 16'b0000000000001100) $display("Shifter do nothing PASS"); else $error("Shifter do nothing FAIL");
				loadc = 1'b0;
				loads = 1'b0;
				#10
				shift = 2'b01;
				loadc = 1'b1;
				loads = 1'b1;
				#10
				assert (datapath_out == 16'b0000000000010100) $display("Shifter multiply by 2 PASS"); else $error("Shifter multiply by 2 FAIL");
				loadc = 1'b0;
				loads = 1'b0;
				#10
				shift = 2'b11;
				loadc = 1'b1;
				loads = 1'b1;
				#10
				assert (datapath_out == 16'b0000000000001000) $display("B shifted right 1-bit, MSB is copy of B[15] PASS"); else $error("B shifted right 1-bit, MSB is copy of B[15] FAIL");
				loadc = 1'b0;
				loads = 1'b0;
				#10
				
				
				
			
				
			$stop;
	end
endmodule