module shifter_tb() ;
	reg [15:0] in ;
	reg [1:0] shift ;
	wire [15:0] out ;
	
	shifter dut(in, shift, out) ;
	
	initial begin
		forever begin
			shift = 2'b00 ;
			#10	;	
			repeat (3) begin
				shift = shift + 1'b1;
				#10 ;
			end
		end
	end
	
	initial begin
		in = 16'b0000000000000000 ;
		#1 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b1111111111111111 ;
		#1 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111110) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0111111111111111) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b1111000011110000 ;
		#1 ;
		assert	(out == 16'b1111000011110000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1110000111100000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0111100001111000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111100001111000) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b0100110011001101 ;
		#1 ;
		assert	(out == 16'b0100110011001101) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1001100110011010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0010011001100110) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0010011001100110) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b1111111011111111 ;
		#1 ;
		assert	(out == 16'b1111111011111111) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111110111111110) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0111111101111111) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111101111111) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b0110001111000001 ;
		#1 ;
		assert	(out == 16'b0110001111000001) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1100011110000010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0011000111100000) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0011000111100000) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b0010110011110101 ;
		#1 ;
		assert	(out == 16'b0010110011110101) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0101100111101010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0001011001111010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0001011001111010) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b1001000011000110 ;
		#1 ;
		assert	(out == 16'b1001000011000110) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0010000110001100) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0100100001100011) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1100100001100011) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b1100110100010101 ;
		#1 ;
		assert	(out == 16'b1100110100010101) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1001101000101010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0110011010001010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1110011010001010) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		in = 16'b0010110000100101 ;
		#1 ;
		assert	(out == 16'b0010110000100101) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0101100001001010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0001011000010010) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0001011000010010) $display("PASS"); else $error("FAIL") ;
		#9 ;
		
		
		$stop ;
	end

endmodule