module PCRAMIR_tb();

	//defining test wires/regs and DUT inputs
	wire [15:0] mdata, IR_out;
	
	reg clk, reset, mwrite, msel, loadpc, loadir;
	reg[15:0] C, B;

	//Instantiating the DUT
	PCRAMIR dut(clk, reset, mwrite, msel, loadpc, loadir, C, B, mdata, IR_out);
	
	
	//Defining CLK
	initial begin
		clk = 0; #5;
		forever begin
			clk = 1; #5;
			clk = 0; #5;
		end
	end
	
	
	//Runing Test
	initial begin
	
	// Seting up initial conditions
		$display("Setting Up Test");
		reset = 1; mwrite = 0; msel = 0; loadpc = 0; loadir = 0;
		#10
		
		
	//Test 1 : reading the first 2 instructions
	$display("Test 1 Started");
		reset = 0;
		#10
		loadir = 1;
		#10
		loadir = 0; loadpc = 1;
		#10
		loadpc = 0;
		#10
		loadir = 1;
		#10
		loadpc = 1; loadir = 0;
		#10
		loadpc = 0;
		#10
		
	//Test 2: Testing writing
	$display("Test 2 Started");
		mwrite = 1; C = 8'b00000010; B = 16'b1100011000000001;
		#10
		mwrite = 0;
		#10
		$stop;
	
	end
endmodule