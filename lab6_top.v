module lab6_top(KEY,LEDR,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,CLOCK_50,SW);

	input [3:0] KEY;
	input [9:0] SW;
	input CLOCK_50;
		
	//input [9:0] SW;
	output [9:0] LEDR; 
	output [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	
	//inputs to computer
	wire clk, reset;
	assign clk = SW[0] ? CLOCK_50 : ~KEY[0];
	assign reset = ~KEY[1];
	
	//outputs of computer
	wire [2:0] status;
	
	assign LEDR[9:0] = {7'b0, status};	
	
	
	
	//PCRAMIR I/O
	wire loadir, mwrite, msel, tsel, incp, execb, isBL;				//loadpc;
	wire [15:0] OutputA, OutputB, OutputC, OutputR0, IROut, mdata;
	wire [7:0] pc_out;
	wire [2:0] cond;
	
	// fill in sseg to display 4-bits in hexidecimal 0,1,2...9,A,B,C,D,E,F
	sseg H0(OutputR0[3:0],   HEX0);   
	sseg H1(OutputR0[7:4],   HEX1);
	sseg H2(OutputR0[11:8],  HEX2);
	sseg H3(OutputR0[15:12], HEX3);
	assign HEX4 = 7'b1111111;  // disabled
	assign HEX5 = 7'b1111111;  // disabled	
	
	
	
	//IRDecode <-> FSM I/O
	wire [1:0] op;
	wire [2:0] opcode, nsel;
	
	
	//IRDecoder <-> Datapath I/O
	wire [1:0] ALUop, shift;
	wire [2:0] readnum, writenum;
	wire [15:0] sximm5, sximm8;
	
	//FSM <-> Datapath I/O
	wire [3:0] vsel;
	wire asel, bsel, loada, loadb, loadc, loads, write;
	
	
	//wires up PC+RAM+IR, Controller, IRDecoder, and Datapath
//	PCRAMIR PcRamIr(clk, reset, mwrite, msel, loadpc, loadir, OutputC, OutputB, mdata, IROut, pc_out														);
	PCRAMIR PcRamIr(clk, reset, mwrite, msel, 		  loadir, OutputC, OutputB, mdata, IROut, pc_out, sximm8, OutputA, tsel, incp, execb, status, cond, isBL);	
	
//	IRDecoder IrDec(IROut, nsel, ALUop, sximm5, sximm8, shift, readnum, writenum, op, opcode	  );
	IRDecoder IrDec(IROut, nsel, ALUop, sximm5, sximm8, shift, readnum, writenum, op, opcode, cond);
	
	datapath DataP(mdata, vsel, asel, bsel, writenum, write, readnum, clk, loada, loadb, loadc, loads, shift, ALUop, status, sximm5, sximm8, OutputA, OutputB, OutputC, OutputR0, pc_out);
	
//	CPU FSM(clk, reset, opcode, op, nsel, loadpc, msel, mwrite, loadir, 				   vsel, loada, loadb, asel, bsel, loadc, loads, write		);  
	CPU FSM(clk, reset, opcode, op, nsel, 		  msel, mwrite, loadir, incp, execb, tsel, vsel, loada, loadb, asel, bsel, loadc, loads, write, isBL); 

	
endmodule


module sseg(in,segs);
	input [3:0] in;
	output [6:0] segs;
	reg [6:0] segs ;

	always @(*) begin
		case(in)
			4'b0000: segs = 7'b1000000 ;
			4'b0001: segs = 7'b1111001 ;
			4'b0010: segs = 7'b0100100 ;
			4'b0011: segs = 7'b0110000 ;
			4'b0100: segs = 7'b0011001 ;
			4'b0101: segs = 7'b0010010 ;
			4'b0110: segs = 7'b0000010 ;
			4'b0111: segs = 7'b1111000 ;
			4'b1000: segs = 7'b0000000 ;
			4'b1001: segs = 7'b0010000 ;
			4'b1010: segs = 7'b0001000 ;
			4'b1011: segs = 7'b0000011 ;
			4'b1100: segs = 7'b1000110 ;
			4'b1101: segs = 7'b0100001 ;
			4'b1110: segs = 7'b0000110 ;
			4'b1111: segs = 7'b0001110 ;
			default: segs = 7'b1111111 ;
		endcase
	end
endmodule