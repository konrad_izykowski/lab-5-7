		MOV R0, #0
		MOV R3, #14
		MOV R1, #10
		MOV R2, #5
		CMP R1, R2
		BNE test1
		B fail
				
test4 :	MOV R0, #10
		HALT
		
fail :	MOV R0, #14
		HALT
	
test2 :	MOV R0, #2
		MOV R7, #50
		STR R3, [R7]
		LDR R4, [R7]
		CMP R4, R1
		BLT fail
		B test3
		B fail

test1 :	MOV R0, #1
		MOV R6, #-5
		ADD R1, R6, R1
		CMP R1, R2
		BEQ test2
		B fail
		
test3 :	MOV R0, #3
		MOV R5, #-1
		MOV R4, #4
		CMP R5, R4 
		BLE test4
		B fail
