module regfile(writenum, write, data_in, clk, readnum, data_out, R0out);
	input [15:0] data_in ;
	input [2:0] writenum, readnum ;
	input write, clk ;
	output [15:0] data_out, R0out ;
		
	wire [7:0] writeNumDec, readNumDec, writeVerify ;
	wire [15:0] R0out, R1out, R2out, R3out, R4out, R5out, R6out, R7out ;
	
//	`define WIDTH 16
	
	Dec #(3,8) decWrite(writenum, writeNumDec) ;
	Dec #(3,8) decRead(readnum, readNumDec) ;
	
	assign writeVerify = (writeNumDec & {8{write}}) ;
	
	
	RegN #(16) R0(data_in, writeVerify[0], clk, R0out) ;
	RegN #(16) R1(data_in, writeVerify[1], clk, R1out) ;
	RegN #(16) R2(data_in, writeVerify[2], clk, R2out) ;
	RegN #(16) R3(data_in, writeVerify[3], clk, R3out) ;
	RegN #(16) R4(data_in, writeVerify[4], clk, R4out) ;
	RegN #(16) R5(data_in, writeVerify[5], clk, R5out) ;
	RegN #(16) R6(data_in, writeVerify[6], clk, R6out) ;
	RegN #(16) R7(data_in, writeVerify[7], clk, R7out) ;
	
	Mux8 #(16) outChoose(R7out, R6out, R5out, R4out, R3out, R2out, R1out, R0out, readNumDec, data_out) ;
	
endmodule


// N-bit register with load enable
module RegN(in, load, clk, out) ;
	parameter n = 1;
	input [n-1:0] in ;
	input load, clk ;
	output [n-1:0] out ;
	wire [n-1:0] chosen, current ;
	
	
	Mux2 #(n) regChoose(current, in, load, chosen) ;
	vDFF #(n) regOut(clk, chosen, current) ;
	
	assign out = current ;
endmodule


// 2 input k-wide mux with one-bit select 
module Mux2(a1, a0, s, b) ;
  parameter k = 1 ;
  input [k-1:0] a0, a1 ;  // inputs
  input s ; // one-bit select
  output[k-1:0] b ;
  wire [k-1:0] b = ({k{s}} & a0) | ({k{~s}} & a1) ;
endmodule


// eight input k-wide mux with one-hot select 
module Mux8(a7, a6, a5, a4, a3, a2, a1, a0, s, b) ;
  parameter k = 1 ;
  input [k-1:0] a0, a1, a2, a3, a4, a5, a6, a7;  // inputs
  input [7:0]   s ; // one-hot select
  output[k-1:0] b ;
  wire [k-1:0] b = ({k{s[0]}} & a0) |
                   ({k{s[1]}} & a1) |
                   ({k{s[2]}} & a2) |
                   ({k{s[3]}} & a3) |
                   ({k{s[4]}} & a4) |
                   ({k{s[5]}} & a5) |
                   ({k{s[6]}} & a6) |
                   ({k{s[7]}} & a7) ;
endmodule


//decoder module (n-bit grey code to m bit one hot)
module Dec(a, b) ;
  parameter n=2 ;
  parameter m=4 ;

  input  [n-1:0] a ;
  output [m-1:0] b ;

  wire [m-1:0] b = 1<<a ;
endmodule


//explicit flip-flop module
module vDFF(clk, in, out) ;
  parameter n = 1;  // width
  input clk ;
  input [n-1:0] in ;
  output [n-1:0] out ;
  reg [n-1:0] out ;

  always @(posedge clk)
    out = in ;

endmodule