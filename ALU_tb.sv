module ALU_tb();

	reg [15:0] Ain;
	reg [15:0] Bin;
	reg [1:0] ALUop;
	wire [15:0] ALUout;
	wire  ALUs;
	
	ALU dut(Ain, Bin, ALUop, ALUout, ALUs);
	
	initial begin
		
		// Tests all operations on 0 numbers
			Ain = 16'b0000000000000000; Bin = 16'b0000000000000000; ALUop = 2'b00;
				#10
				ALUop = 2'b00;
				#5
				$display("%b + %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000000, 1'b1);
				assert (ALUout == 16'b0000000000000000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b1) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b01;
				#5
				$display("%b - %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000000, 1'b1);
				assert (ALUout == 16'b0000000000000000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b1) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b10;
				#5
				$display("%b & %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000000, 1'b1);
				assert (ALUout == 16'b0000000000000000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b1) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b11;
				#5
				$display(" ~%b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Bin, ALUout, ALUs, 16'b1111111111111111, 1'b0);
				assert (ALUout == 16'b1111111111111111) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#10
				
		// Tests all operations on a random A number and B 0	
			Ain = 16'b1010000011011001; Bin = 16'b0000000000000000; ALUop = 2'b00;
				#10
				ALUop = 2'b00;
				#5
				$display("%b + %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1010000011011001, 1'b0);
				assert (ALUout == 16'b1010000011011001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b01;
				#5
				$display("%b - %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1010000011011001, 1'b0);
				assert (ALUout == 16'b1010000011011001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b10;
				#5
				$display("%b & %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000000, 1'b1);
				assert (ALUout == 16'b0000000000000000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b1) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b11;
				#5
				$display("~%b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Bin, ALUout, ALUs, 16'b1111111111111111, 1'b0);
				assert (ALUout == 16'b1111111111111111) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#10
				
		// Tests all operations on a random B number and A 0	
			Ain = 16'b0000000000000000; Bin = 16'b0010110010100001; ALUop = 2'b00;
				#10
				ALUop = 2'b00;
				#5
				$display("%b + %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0010110010100001, 1'b0);
				assert (ALUout == 16'b0010110010100001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b01;
				#5
				$display("%b - %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0010110010100001, 1'b0);
				assert (ALUout == 16'b0010110010100001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b10;
				#5
				$display("%b & %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000000, 1'b1);
				assert (ALUout == 16'b0000000000000000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b1) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b11;
				#5
				$display("~%b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Bin, ALUout, ALUs, 16'b1101001101011110, 1'b0);
				assert (ALUout == 16'b1101001101011110) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#10
				
		// Tests all operations on a random number A and B	where A < B
			Ain = 16'b0000000000000111; Bin = 16'b1111110010100001; ALUop = 2'b00;
				#10
				ALUop = 2'b00;
				#5
				$display("%b + %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1111110010101000, 1'b0);
				assert (ALUout == 16'b1111110010101000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b01;
				#5
				$display("%b - %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1111110010101000, 1'b0);
				assert (ALUout == 16'b1111110010101000) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b10;
				#5
				$display("%b & %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000001, 1'b0);
				assert (ALUout == 16'b0000000000000001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b11;
				#5
				$display("~%b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Bin, ALUout, ALUs, 16'b0000001101011110, 1'b0);
				assert (ALUout == 16'b0000001101011110) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#10
				
		// Tests all operations on a random number A and B	where A > B
			Ain = 16'b1010010100001101; Bin = 16'b0000000000110001; ALUop = 2'b00;
				#10
				ALUop = 2'b00;
				#5
				$display("%b + %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1010010100111110, 1'b0);
				assert (ALUout == 16'b1010010100111110) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b01;
				#5
				$display("%b - %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b1010010100111110, 1'b0);
				assert (ALUout == 16'b1010010100111110) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b10;
				#5
				$display("%b & %b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Ain, Bin, ALUout, ALUs, 16'b0000000000000001, 1'b0);
				assert (ALUout == 16'b0000000000000001) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#1
				ALUop = 2'b11;
				#5
				$display("~%b -> %b, Status: %b  || Expected Result:%b   Expected Status:%b", Bin, ALUout, ALUs, 16'b1111111111001110, 1'b0);
				assert (ALUout == 16'b1111111111001110) $display("Result PASS"); else $error("Result FAIL");
				assert (ALUs == 1'b0) $display("Status PASS"); else $error("Status FAIL");
				#10
		$stop;
		
	end
endmodule