module IRDecoder(IR, nsel, ALUop, sximm5, sximm8, shift, readnum, writenum, op, opcode, cond);
	
	
	//Defining I/O for the module
	input [15:0] IR;
	input [2:0] nsel;
	
	
	output [15:0] sximm5, sximm8;
	output [2:0] readnum, writenum, opcode, cond;
	output [1:0] ALUop, shift, op;
	
	assign ALUop = IR[12:11];
	assign sximm5 =  IR[4] ? {11'b11111111111,IR[4:0]} : {11'b00000000000,IR[4:0]};
	assign sximm8 = IR[7] ? {8'b11111111,IR[7:0]} : {8'b00000000,IR[7:0]};
	assign shift = IR[4:3];
	assign cond = IR[10:8];

	Mux3 #(3) Num_Select(IR[10:8], IR[7:5], IR[2:0], nsel,readnum);
	assign writenum = readnum;
	
	assign opcode = IR[15:13];
	assign op = IR[12:11];

endmodule