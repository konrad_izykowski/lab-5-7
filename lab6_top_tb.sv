module lab6_top_tb();
	
	
	reg [3:0] KEY;
	reg [9:0] SW;
	reg CLOCK_50;
	
	
	wire [9:0] LEDR;
	wire [6:0] HEX0,HEX1,HEX2,HEX3,HEX4,HEX5;
	
	//Instantiating the DUT
	lab6_top DUT(KEY,LEDR,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,CLOCK_50,SW);
	
	
	//Defining CLK
	initial begin
		CLOCK_50 = 0; #2;
		forever begin
			CLOCK_50 = 1; #2;
			CLOCK_50 = 0; #2;
		end
	end
	
	
	//Running Main test
	initial begin
	
	// Seting up initial conditions
	$display("Setting Up Test");
		KEY[1] = 0; SW[0] = 1;
		#4
		KEY[1] = 1;
	//Test 1 move values 0 to 8 to each of the 8 registers (Finishes at 134 )
	$display("Test1 : placing a number in each of the eight register files");
	#132
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R0out, 16'b0 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R0out == 16'b0) $display("Pass: R0 contains 0"); else $error("Fail: R0 does not contains 0");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R1out, 16'b0000000000000001 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R1out == 16'b0000000000000001) $display("Pass: R1 contains 1"); else $error("Fail: R1 does not contains 1");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R2out, 16'b0000000000000010 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R2out == 16'b0000000000000010) $display("Pass: R2 contains 2"); else $error("Fail: R2 does not contains 2");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R3out, 16'b0000000000000011 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R3out == 16'b0000000000000011) $display("Pass: R3 contains 3"); else $error("Fail: R3 does not contains 3");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R4out, 16'b0000000000000100 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R4out == 16'b0000000000000100) $display("Pass: R4 contains 4"); else $error("Fail: R4 does not contains 4");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R5out, 16'b0000000000000101 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R5out == 16'b0000000000000101) $display("Pass: R5 contains 5"); else $error("Fail: R5 does not contains 5");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R6out, 16'b0000000000000110 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R6out == 16'b0000000000000110) $display("Pass: R6 contains 6"); else $error("Fail: R6 does not contains 6");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R7out, 16'b0000000000000111 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R7out == 16'b0000000000000111) $display("Pass: R7 contains 7"); else $error("Fail: R7 does not contains 7");
	
	//Test 2: ALU ADD r0 r1 r6 (stops at 158)
	$display("Test2 : ALU ADD r0 r1 r6");
	#26
		$display("Current R0: %b | Expected R0: %b", lab6_top_tb.DUT.DataP.Register_File.R0out, 16'b0000000000000111 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R0out == 16'b0000000000000111) $display("Pass: R0 contains 7"); else $error("Fail: R0 does not contains 7");
		assert (HEX0 == 7'b1111000) $display("HEX0 Pass"); else $error("HEX0 Fail");
		assert (HEX1 == 7'b1000000) $display("HEX1 Pass"); else $error("HEX1 Fail");
		assert (HEX2 == 7'b1000000) $display("HEX2 Pass"); else $error("HEX2 Fail");
		assert (HEX3 == 7'b1000000) $display("HEX3 Pass"); else $error("HEX3 Fail");
		//assert (LEDR == 3'b000) $display("LEDR Pass"); else $error("LEDR Fail");
		
	//Test 3: ALU AND r1 r0 r5 (ends at 182)
	$display("Test3 : ALU AND r1 r0 r5 rsult should be 5 ");
	#24
		$display("Current R1: %b | Expected R1: %b", lab6_top_tb.DUT.DataP.Register_File.R1out, 16'b0000000000000101 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R1out == 16'b0000000000000101) $display("Pass: R1 contains 5"); else $error("Fail: R1 does not contains 5");
		assert (HEX0 == 7'b0010010) $display("HEX0 Pass"); else $error("HEX0 Fail");
		assert (HEX1 == 7'b1000000) $display("HEX1 Pass"); else $error("HEX1 Fail");
		assert (HEX2 == 7'b1000000) $display("HEX2 Pass"); else $error("HEX2 Fail");
		assert (HEX3 == 7'b1000000) $display("HEX3 Pass"); else $error("HEX3 Fail");
		
	//Test 4: ALU MVM r2 r3 ends at 206)
	$display("Test4 : ALU MVM r3 r4");
	#24
		$display("Current R2: %b | Expected R2: %b", lab6_top_tb.DUT.DataP.Register_File.R2out, 16'b1111111111111100 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R2out == 16'b1111111111111100) $display("Pass: R2 contains ~R3"); else $error("Fail: R1 does not contains ~R3");
		assert (HEX0 == 7'b1000110) $display("HEX0 Pass"); else $error("HEX0 Fail");
		assert (HEX1 == 7'b0001110) $display("HEX1 Pass"); else $error("HEX1 Fail");
		assert (HEX2 == 7'b0001110) $display("HEX2 Pass"); else $error("HEX2 Fail");
		assert (HEX3 == 7'b0001110) $display("HEX3 Pass"); else $error("HEX3 Fail");
	
	//Test 5: LDR r3 [r5, #15] (ends at 230)
	$display("Test5 : LDR r3 [r5, #15]");
	#24
		$display("Current R3: %b | Expected R3: %b", lab6_top_tb.DUT.DataP.Register_File.R3out, 16'b0000000001101001 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R3out == 16'b0000000001101001) $display("Pass: LDR loaded 105 in R3"); else $error("Fail: LDR did not load 105 in R3");
	
	
	//Test 6: STR (ends at 246)
	$display("Test 6 : storing r7 into r0+15");
	#16
		assert (lab6_top_tb.DUT.PcRamIr.B == 16'b0000000000000111) $display("Pass: STR Stored 7"); else $error("Fail: STR did not Store 7");
		
	//Test 7: LDR r4 [r7, #15]  to test str (ends)
	$display("Test7 : LDR r4 [r7, #15]");
	#36
		$display("Current R4: %b | Expected R4: %b", lab6_top_tb.DUT.DataP.Register_File.R4out, 16'b0000000000000111 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R4out == 16'b0000000000000111) $display("Pass: LDR loaded 7in R4"); else $error("Fail: LDR did not load 7 in R3");
		
	
	
	//KONRAD NEED TEXT FILE
	/*$display("Test6 : puts neg in R6, R5, adds them in R7, negates them in R7");
		#70
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R6out, 16'b1111111111100111 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R6out == 16'b1111111111100111) $display("Pass: R6 correct"); else $error("Fail: R0 does not contains 0");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R5out, 16'b1111111110101100 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R5out == 16'b1111111110101100) $display("Pass: R5 correct"); else $error("Fail: R1 does not contains 1");
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R7out, 16'b1111111110010011 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R7out == 16'b1111111110010011) $display("Pass: R7 correct"); else $error("Fail: R2 does not contains 2");
		#13
		$display("Current State: %b | Expected State: %b", lab6_top_tb.DUT.DataP.Register_File.R3out, 16'b0000000001101100 );
		assert (lab6_top_tb.DUT.DataP.Register_File.R3out == 16'b0000000001101100) $display("Pass: R7 correct"); else $error("Fail: R3 does not contains 3");
		*/
		
	#1500
	$stop;
	
	end
	
	

endmodule