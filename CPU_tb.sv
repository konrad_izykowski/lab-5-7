module CPU_tb();

	//defining test wires/regs and DUT inputs
	wire [3:0] vsel;
	wire [2:0] nsel;
	wire loadpc, msel, mwrite, loadir,  loada, loadb, asel, bsel, loadc, loads, write;
	
	reg clk, reset;
	reg [2:0] opcode;
	reg [1:0] op;

	//Instantiating the DUT
	CPU DUT(clk, reset, opcode, op, nsel, loadpc, msel, mwrite, loadir, vsel, loada, loadb, asel, bsel, loadc, loads, write);
	
	//Defining States
	`define SLIR 4'b0000
	`define SLPC 4'b0001
	`define SCDC 4'b0010
	`define SWRN 4'b0011
	`define SRRM 4'b0100
	`define SALU 4'b0101
	`define SRFR 4'b0110
	`define SWTR 4'b0111
	`define SR   4'b1111
	
	//Defining CLK
	initial begin
		clk = 0; #5;
		forever begin
			clk = 1; #5;
			clk = 0; #5;
		end
	end
	
	initial begin
	
	// Reseting state machine
	$display("Setting Up Test");
		reset = 1;  opcode = 3'b110; op = 2'b10;
		#10
		
	//Test 1: First MOV Test
	$display("***************Started Test 1: First MOV Test*****************");
	reset = 0;   opcode = 3'b110; op = 2'b10;
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 2: ADD Test
	$display("***************Started Test 2: ADD Test*****************");
	opcode = 3'b101; op = 2'b00;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 3: ADD Test
	$display("***************Started Test 3: CMP Test*****************");
	opcode = 3'b101; op = 2'b01;
	$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 4: AND Test
	$display("***************Started Test 4: AND Test*****************");
	opcode = 3'b101; op = 2'b10;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 5: MVM Test
	$display("***************Started Test 5: MVM Test*****************");
	opcode = 3'b101; op = 2'b11;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 6: Second Move Test
	$display("***************Started Test 6: Second Move Test*****************");
	opcode = 3'b110; op = 2'b00;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 7: LDR
	$display("***************Started Test 7: LDR*****************");
	opcode = 3'b011; op = 2'b00;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRFR );
		assert (CPU_tb.DUT.state == `SRFR) $display("Pass: State SRFR"); else $error("Fail: State not SRFR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWRN );
		assert (CPU_tb.DUT.state == `SWRN) $display("Pass: State SWRN"); else $error("Fail: State not SWRN");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
	//Test 8: STR
	$display("***************Started Test 8: STR*****************");
	opcode = 3'b100; op = 2'b00;
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLPC );
		assert (CPU_tb.DUT.state == `SLPC) $display("Pass: State SLPC"); else $error("Fail: State not SLPC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SCDC );
		assert (CPU_tb.DUT.state == `SCDC) $display("Pass: State SCDC"); else $error("Fail: State not SCDC");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SRRM );
		assert (CPU_tb.DUT.state == `SRRM) $display("Pass: State SRRM"); else $error("Fail: State not SRRM");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SALU );
		assert (CPU_tb.DUT.state == `SALU) $display("Pass: State SALU"); else $error("Fail: State not SALU");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SWTR );
		assert (CPU_tb.DUT.state == `SWTR) $display("Pass: State SWTR"); else $error("Fail: State not SWTR");
		#10
		$display("Current State: %b | Expected State: %b", CPU_tb.DUT.state, `SLIR );
		assert (CPU_tb.DUT.state == `SLIR) $display("Pass: State SLIR"); else $error("Fail: State not SLIR");
		
		
	#5
	$stop;
	end
	
endmodule