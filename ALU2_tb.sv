module ALU2_tb() ;
	reg [15:0] Ain, Bin ;
	reg [1:0] ALUop ;
	wire [15:0] out ;
	wire [2:0] status ;
	
	ALU dut(Ain, Bin, ALUop, out, status);
	
	initial begin
		forever begin
			ALUop = 2'b00 ;
			#10	;	
			repeat (3) begin
				ALUop = ALUop + 1'b1;
				#10 ;
			end
		end
	end
	
	initial begin
		Ain = 16'b0000000000000000 ; Bin = 16'b0000000000000000 ; 
		#1 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b0000000000000000 ; Bin = 16'b1111111111111111 ; 
		#1 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000001) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b1111111111111111 ; Bin = 16'b0000000000000000 ; 
		#1 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b1111111111111111 ; Bin = 16'b1111111111111111 ; 
		#1 ;
		assert	(out == 16'b1111111111111110) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b0111111111111111 ; Bin = 16'b0111111111111111 ; 
		#1 ;
		assert	(out == 16'b1111111111111110) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b1000000000000000 ; Bin = 16'b0111111111111111 ; 
		#1 ;
		assert	(out == 16'b1111111111111111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000001) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b1) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1000000000000000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b0110010001110011 ; Bin = 16'b1001110100011101 ;
		#1 ;
		assert	(out == 16'b0000000110010000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1100011101010110) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0000010000010001) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0110001011100010) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		Ain = 16'b1001001100110100 ; 
		Bin = 16'b1101000111100011 ; 
		#1 ;
		assert	(out == 16'b0110010100010111) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1100000101010001) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b1001000100100000) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b1) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#10 ;
		assert	(out == 16'b0010111000011100) $display("PASS"); else $error("FAIL") ;
		assert	(status[2] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[1] == 3'b0) $display("PASS"); else $error("FAIL") ;
		assert	(status[0] == 3'b0) $display("PASS"); else $error("FAIL") ;
		#9
		
		$stop ;
	end

endmodule